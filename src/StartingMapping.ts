import * as fs from 'fs'
import * as lo_ from 'lodash'
//import  {OriginJSON, CustObjectiveOnPositionElement} from './types'

let rawdata = fs.readFileSync('C:/work/ecopsy/edu/typescript/mapping/files/PMGM003_Objectives_Tree-1572520336714.json', "utf8");
const myJson = JSON.parse(rawdata);

let arr = myJson.cust_ObjectiveOnPosition.cust_ObjectiveOnPosition;
if (!(myJson && myJson.cust_ObjectiveOnPosition && myJson.cust_ObjectiveOnPosition.cust_ObjectiveOnPosition)) {
    throw "ERROR!!!"
}


function removeDuplicates(array, key) { //functionforremovingduplicates
    let lookup = {};
    let result = [];
    for (let i = 0; i < array.length; i++) {
        if (!lookup[array[i][key]]) {
            lookup[array[i][key]] = true;
            result.push(array[i]);
        }
    }
    return result;
}
let groups = []; // array with duplicate positions 
let nodes = [];
let lines = [];
let arrLinesAndNodes=[];
let arrPicklist=[];
for (let _i = 0; _i < arr.length; _i++) {
    groups.push({
        'key': arr[_i].cust_ToPositions.Position.code,
        'title': arr[_i].cust_ToPositions.Position.positionTitle
    })
   groups = removeDuplicates(groups, 'key');
   arrLinesAndNodes.push(arr[_i].cust_ToObjectiveOnPositionDetails.cust_ObjectiveOnPositionDetails)
   arrPicklist.push(arr[_i].cust_ObjectivePlanTypeNav.PickListValueV2)
console.log(arrLinesAndNodes)
   for ( let _j=0; _j<arrLinesAndNodes.length;_j++ ){
       if    (arrLinesAndNodes[_j].cust_ParentObjective)
       { lines.push({
        'from': arrLinesAndNodes[_j].cust_ParentObjective,
        'to': arrLinesAndNodes[_j].externalCode
       })
       }

   
    // origin object for nodes    
    let obj={
            'key': arrLinesAndNodes[_j].externalCode,
            'title': arrLinesAndNodes[_j].cust_GoalName,
            'group':arr[_i].cust_ToPositions.Position.code,
            'shape':'Box'
         }
    // object for functionalplan 1002
      let objfunctional={
          'Категория': arrLinesAndNodes[_j].cust_GoalCategory,
          'Плановый результат': arrLinesAndNodes[_j].cust_ResultDescription,
          'Срок выполнения': arrLinesAndNodes[_j].cust_ResultDate,
          'Методика': arrLinesAndNodes[_j].cust_ResultMethod
      }  
     //object for measurement 1001
      let objmeasurement={
          'Единица измерения': arrLinesAndNodes[_j].cust_Metric,
          'Плановое значение': arrLinesAndNodes[_j].cust_PlannedAmout,
          'Формула расчета': arrLinesAndNodes[_j].cust_Formula

      }
    
    //object for service 1005
      let objservice={
          'Плановый результат': arrLinesAndNodes[_j].cust_ResultDescription,
          'Срок выполнения': arrLinesAndNodes[_j].cust_ResultDate,
          'Методика': arrLinesAndNodes[_j].cust_ResultMethod
      }
    
    //object for key 1003
      let objkey={
          'Плановый результат': arrLinesAndNodes[_j].cust_ResultDescription,
          'Срок выполнения': arrLinesAndNodes[_j].cust_ResultDate,
          'Методика': arrLinesAndNodes[_j].cust_ResultMethod,
          'Вес': arrLinesAndNodes[_j].cust_Weight
      }

      


     if (arrPicklist[_i]&&arrPicklist[_i].externalCode=='1001')
     {
         obj['Атрибут']= objmeasurement
         obj['icon']='sap-icon://compare'
     }
     else if (arrPicklist[_i]&&arrPicklist[_i].externalCode=='1002')
     {
        obj['Атрибут']= objfunctional,
        obj['icon']='sap-icon://target-group'
     }
     else if (arrPicklist[_i]&&arrPicklist[_i].externalCode=='1003')
     {
        obj['Атрибут']= objkey,
        obj['icon']='sap-icon://two-keys'
     }
     else if (arrPicklist[_i]&&arrPicklist[_i].externalCode=='1005')
     {
        obj['Атрибут']= objservice,
        obj['icon']='sap-icon://kpi-managing-my-area'

     }
       nodes.push(obj)
  // console.log(arr)

    }

}
let objGeneral = {
    "nodes": nodes,
    "lines": lines,
    "groups": groups
}


console.log(JSON.stringify(objGeneral, null, 4))